#include "Box.h"
#include "Engine/Model.h"
#include "Engine/Input.h"

//コンストラクタ
Box::Box(GameObject* parent)
    :GameObject(parent, "Box"), hModel_(-1)
{

}

//デストラクタ
Box::~Box()
{
}

//初期化
void Box::Initialize()
{
    //モデルデータのロード
    hModel_ = Model::Load("box.fbx");
    assert(hModel_ >= 0);

    transform_.position_.z = 10;
}

//更新
void Box::Update()
{

    //スペースキーが押されていたら
    if (Input::IsKey(DIK_DOWN))
    {
        transform_.rotate_.x += -3;
        transform_.position_.z -= 0.05;
    }
}

//描画
void Box::Draw()
{
    Model::SetTransform(hModel_, transform_);
    Model::Draw(hModel_);
}

//開放
void Box::Release()
{
}