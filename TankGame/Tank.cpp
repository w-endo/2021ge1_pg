#include "Tank.h"
#include "Engine/Model.h"
#include "Engine/Input.h"
#include "Cannon.h"
#include "Engine/Camera.h"
#include "Ground.h"

//コンストラクタ
Tank::Tank(GameObject* parent)
    :GameObject(parent, "Tank"),

    //定数
    RUN_SPEED(0.1f),
    ROTATE_SPEED(3.0f),
    FIXED_CAM_POS(XMFLOAT3(0, 10, -20)),
    TP_CAM_Y(8.0f), TP_CAM_Z(-10.0f),

    //変数
    hModel_(-1),
    cameraType_(CAM_TP_A)
{
}

//デストラクタ
Tank::~Tank()
{
}

//初期化
void Tank::Initialize()
{
    //モデルデータのロード
    hModel_ = Model::Load("TankBase.fbx");
    assert(hModel_ >= 0);


    Instantiate<Cannon>(this);
}

//更新
void Tank::Update()
{

    //////////////////////////　戦車の向きを変える　/////////////////////////////////
    //左回転
    if (Input::IsKey(DIK_A))
    {
        transform_.rotate_.y -= ROTATE_SPEED;
    }

    //右回転
    if (Input::IsKey(DIK_D))
    {
        transform_.rotate_.y += ROTATE_SPEED;
    }





    //////////////////////////　この後の処理で汎用的に使うものを準備しておく　/////////////////////////////////

    //transform_.rotate_.yの値に合わせてＹ軸回転させる行列
    XMMATRIX mRotate = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.y));

    //現在位置をベクトルにしておく
    XMVECTOR vPos = XMLoadFloat3(&transform_.position_);

    //移動ベクトル
    XMFLOAT3 move = { 0, 0, RUN_SPEED };                //奥向きのXMFLOAT3構造体を用意し
    XMVECTOR vMove = XMLoadFloat3(&move);               //それをベクトルにする
    vMove = XMVector3TransformCoord(vMove, mRotate);    //戦車の向きに合わせて回転






    //////////////////////////　前進　/////////////////////////////////
    if (Input::IsKey(DIK_W))
    {
        //移動ベクトルを現在地に加算
        vPos += vMove;
        XMStoreFloat3(&transform_.position_, vPos);
    }



    Ground* pGround = (Ground*)FindObject("Ground");    //ステージオブジェクトを探す
    int hGroundModel = pGround->GetModelHandle();    //モデル番号を取得

    RayCastData data;
    data.start = transform_.position_;   //レイの発射位置
    data.start.y = 0;
    data.dir = XMFLOAT3(0, -1, 0);       //レイの方向
    Model::RayCast(hGroundModel, &data); //レイを発射
    
    if (data.hit)
    {
        transform_.position_.y = -data.dist;
    }


    //////////////////////////　カメラ切り替え　/////////////////////////////////
    if (Input::IsKeyDown(DIK_TAB))
    {
        int ct = cameraType_;
        ct++;
        if (ct >= CAM_MAX)
        {
            ct = 0;
        }
        cameraType_ = (CameraType)ct;
    }



    

    //////////////////////////　各種カメラの設定　/////////////////////////////////
    switch (cameraType_)
    {
    //固定カメラ
    case CAM_FIXED:
        Camera::SetPosition(FIXED_CAM_POS);     //カメラの位置は指定位置に固定
        Camera::SetTarget(XMFLOAT3(0, 0, 0));           //カメラの焦点はワールド原点に固定
        break;


    //位置は固定、戦車の方を向く
    case CAM_CHASE:
        Camera::SetPosition(FIXED_CAM_POS);     //カメラの位置は指定位置に固定
        Camera::SetTarget(transform_.position_);        //カメラの焦点はプレイヤーの位置
        break;


    //三人称視点Ａ
    case CAM_TP_A:
        XMFLOAT3 cam = transform_.position_;            //現在のプレーヤーの位置を…
        cam.y += TP_CAM_Y;                               //上と
        cam.z -= TP_CAM_Z;                               //後ろにずらし
        Camera::SetPosition(cam);                       //そこをカメラの位置にする
        Camera::SetTarget(transform_.position_);        //カメラの焦点はプレイヤーの位置
        break;


    //三人称視点B
    case CAM_TP_B:
        XMVECTOR vCam = XMVectorSet(0, TP_CAM_Y, TP_CAM_Z, 0);    //後ろ上方に伸びるベクトルを用意
        vCam = XMVector3TransformCoord(vCam, mRotate);                  //それを戦車の向きに合わせて回転
        XMFLOAT3 camPos;
        XMStoreFloat3(&camPos, vPos + vCam);                            //現在の位置に、上記のベクトルを足し
        Camera::SetPosition(camPos);                                    //そこをカメラの位置にする
        Camera::SetTarget(transform_.position_);                        //カメラの焦点はプレイヤーの位置
        break;


    //一人称視点
    case CAM_FP:
        Camera::SetPosition(transform_.position_);          //カメラの位置は戦車の位置
        XMFLOAT3 camTarget;
        XMStoreFloat3(&camTarget, vPos + vMove);            //現在の戦車の位置に移動ベクトルを加算し…（これで進行方向の位置になる）
        Camera::SetTarget(camTarget);                       //そこを焦点にする
        break;
    }
    
}

//描画
void Tank::Draw()
{
    Model::SetTransform(hModel_, transform_);
    Model::Draw(hModel_);
}

//開放
void Tank::Release()
{
}