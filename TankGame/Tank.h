﻿#pragma once
#include "Engine/GameObject.h"

//戦車本体を管理するクラス
class Tank : public GameObject
{
    //定数
    const float     RUN_SPEED;          //移動速度
    const float     ROTATE_SPEED;       //回転速度
    const XMFLOAT3  FIXED_CAM_POS;      //固定カメラの位置
    const float     TP_CAM_Y, TP_CAM_Z; //三人称視点時の、プレイヤーから見たカメラ位置


    enum CameraType
    {
        CAM_FIXED,  //固定カメラ
        CAM_CHASE,  //位置は固定、戦車の方を向く
        CAM_TP_A,   //三人称視点Ａ
        CAM_TP_B,   //三人称視点Ｂ
        CAM_FP,     //一人称視点
        CAM_MAX
    };


    //メンバ変数
    int hModel_;        //モデル番号
    CameraType cameraType_;    //カメラの種類

public:
    //コンストラクタ
    Tank(GameObject* parent);

    //デストラクタ
    ~Tank();

    //初期化
    void Initialize() override;

    //更新
    void Update() override;

    //描画
    void Draw() override;

    //開放
    void Release() override;
};