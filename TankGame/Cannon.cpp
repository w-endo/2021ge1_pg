#include "Cannon.h"
#include "Engine/Model.h"
#include "Engine/Input.h"
#include "Bullet.h"



//コンストラクタ
Cannon::Cannon(GameObject* parent)
    :GameObject(parent, "Cannon"), ROTATE_SPEED(3.0f), hModel_(-1)
{
}

//デストラクタ
Cannon::~Cannon()
{
}

//初期化
void Cannon::Initialize()
{
    //モデルデータのロード
    hModel_ = Model::Load("TankHead.fbx");
    assert(hModel_ >= 0);
}

//更新
void Cannon::Update()
{
    if (Input::IsKey(DIK_LEFT))
    {
        transform_.rotate_.y -= ROTATE_SPEED;
    }

    if (Input::IsKey(DIK_RIGHT))
    {
        transform_.rotate_.y += ROTATE_SPEED;
    }

    //弾を打つ
    if (Input::IsKeyDown(DIK_SPACE))
    {
        Bullet* pBullet = Instantiate<Bullet>(GetParent()->GetParent());

        XMFLOAT3 shotPos = Model::GetBonePosition(hModel_, "ShotPos");      //大砲の先端位置
        XMFLOAT3 cannonRoot = Model::GetBonePosition(hModel_, "CannonRoot");//大砲の根元位置

        //�@ ShotPosをXMVECTORにする
        XMVECTOR vShotPos = XMLoadFloat3(&shotPos);
         
        //�A CannonRootをXMVECTORにする
        XMVECTOR vCannonPos = XMLoadFloat3(&cannonRoot);

        //�B �@−�Aを求める（これもXMVECTOR型）
        XMVECTOR vMove = vShotPos - vCannonPos;
        vMove = XMVector3Normalize(vMove);  //ベクトルの長さを1にしてから
        vMove *= 0.5f;                      //長さを0.5にする

        //�C �BをXMFLOAT3型に変換
        XMFLOAT3 move;
        XMStoreFloat3(&move, vMove);

        pBullet->SetPosition(shotPos);
        pBullet->SetMove(move);
    }
    
}

//描画
void Cannon::Draw()
{
    Model::SetTransform(hModel_, transform_);
    Model::Draw(hModel_);
}

//開放
void Cannon::Release()
{
}