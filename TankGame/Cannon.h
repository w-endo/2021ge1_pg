﻿#pragma once
#include "Engine/GameObject.h"

//砲台（戦車の上半分）を管理するクラス
class Cannon : public GameObject
{
    //定数
    const float ROTATE_SPEED;   //回転速度


    //変数
    int hModel_;    //モデル番号

public:
    //コンストラクタ
    Cannon(GameObject* parent);

    //デストラクタ
    ~Cannon();

    //初期化
    void Initialize() override;

    //更新
    void Update() override;

    //描画
    void Draw() override;

    //開放
    void Release() override;
};