#include "Enemy.h"
#include "Engine/Model.h"
#include "Engine/SphereCollider.h"

//コンストラクタ
Enemy::Enemy(GameObject* parent)
    :GameObject(parent, "Enemy"), hModel_(-1)
{
}

//デストラクタ
Enemy::~Enemy()
{
}

//初期化
void Enemy::Initialize()
{
    transform_.position_.x = (float)(rand() % 4001 - 2000) / 100.0f;
    transform_.position_.z = 30;

    //モデルデータのロード
    hModel_ = Model::Load("Enemy.fbx");
    assert(hModel_ >= 0);

    SphereCollider* collision = new SphereCollider(XMFLOAT3(0.0f, 0, 0), 0.8f);
    AddCollider(collision);
}

//更新
void Enemy::Update()
{
 
}

//描画
void Enemy::Draw()
{
    Model::SetTransform(hModel_, transform_);
    Model::Draw(hModel_);
}

//開放
void Enemy::Release()
{
}

//何かに当たった
void Enemy::OnCollision(GameObject* pTarget)
{
    //当たったときの処理

    if (pTarget->GetObjectName() == "Bullet")
    {
        KillMe();
        pTarget->KillMe();
    }
}