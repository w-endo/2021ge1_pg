#include "Bullet.h"
#include "Engine/Model.h"
#include "Engine/SphereCollider.h"

//コンストラクタ
Bullet::Bullet(GameObject* parent)
    :GameObject(parent, "Bullet"), hModel_(-1)
{
}

//デストラクタ
Bullet::~Bullet()
{
}

//初期化
void Bullet::Initialize()
{
    //モデルデータのロード
    hModel_ = Model::Load("bullet.fbx");
    assert(hModel_ >= 0);

    transform_.scale_.x = 0.4f;
    transform_.scale_.y = 0.4f;
    transform_.scale_.z = 0.4f;

    SphereCollider* collision = new SphereCollider(XMFLOAT3(0.0f, 0, 0), 0.5f);
    AddCollider(collision);
}

//更新
void Bullet::Update()
{
    transform_.position_.z += 0.5f;

    if (transform_.position_.z >= 50)
    {
        KillMe();
    }
}

//描画
void Bullet::Draw()
{
    Model::SetTransform(hModel_, transform_);
    Model::Draw(hModel_);
}

//開放
void Bullet::Release()
{
}